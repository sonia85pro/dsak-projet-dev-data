import Vue from "vue";
import Router from "vue-router";
// import Forme from "./components/forme.vue";
import Login from "./components/login.vue";
import Chat from "./components/chat.vue";

//import 'bootstrap';
//import 'bootstrap/dist/css/boostrap.min.css';
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    //     path: "/",
    //     name: "forme",
    //     component: Forme
    //   },
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat,
      props: true,
      beforeEnter: (to, from, next) => {
        if (to.params.name) {
          next(); //; j ai l enleve
        } else {
          next({
            name: 'Login' //peut etre majuscule
          })
        }
      }
    },
    // },
    // {
    //   path: "/forme",
    //   name: "forme",
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import("./components/Forme.vue")
    // }
  ]

});